# Projeto Robô para Soluts Robot Arena
## Sentinela
 **Movimentação:** Sua movimentação foi pensada em evitar colidir com a parede, ao se aproximar da parede o metodo de reverter a direção é chamado. Inicialmete começa andando em linha reta para frente e ao encontrar um inimigo passa a andar em seu entorno.
&nbsp;

**Ações:** Seu radar realiza uma volta completa atrás de inimigos, quando o scaner localiza o inimigo, foca nele com o canhão e o scanner, então realiza verificações de distancia para tomar a melhor decisão:
&nbsp;

Se estiver mais proximo que 80 px, recua em 100 px.
&nbsp;

Se estiver mais proximo que 200 px, Atira com força 3.
&nbsp;

Se estiver mais proximo que 400 px e mais distante que 200 px, se aproxima rondando o inimigo enquanto atira com força 1.
&nbsp;

E se estiver mais distante que 400 px se aproxima enquanto atira com força 1.
&nbsp;

Ao ocorrer uma **colisão com o inimigo**, o canhão será voltado para o lado da colisão e atirar com força 3.
&nbsp;

E ao **colidir com a parede** ou com uma bala, o metodo de reverter diração é chamado.
&nbsp;

## Prós e contras

**Prós**: Está sempre em movimento evitando colisão com objetos mudando a intensidade
de diaparo de acordo com a distancia do inimigo

**Contras**: Com inimigos cuja tatica é ficar parado, como por exemplo o Corners (um dos robô padrões da plataforma) acaba fazendo o Sentinela parar também, ficando vuneravel.
 

 